/**
 * componentExists
 *
 * Check whether the given component exist in either the components or containers directory
 */

const fs = require('fs');
const path = require('path');
const directories = [
  path.join(__dirname, '../../../app/components'),
  path.join(__dirname, '../../../app/components'),
  path.join(__dirname, '../../../app/pages'),
  path.join(__dirname, '../../../app/screens'),
];

const allComponents = directories.reduce((finalList, item) => {
  if (fs.existsSync(item)) {
    finalList.push(fs.readdirSync(item));
  }
  return finalList;
}, []);

function componentExists(comp) {
  return allComponents.indexOf(comp) >= 0;
}

module.exports = componentExists;
