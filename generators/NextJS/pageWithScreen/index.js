/**
 * Component Generator
 */
const componentExists = require("../../../utils/componentExists");

module.exports = {
  description: "Add new page and screen in your next app",
  prompts: [
    {
      type: "input",
      name: "name",
      message: "What should it be called?",
      default: "Home",
    },
    {
      type: "input",
      name: "route",
      message:
        'Enter the route for page (e.g : "Home/index" or "Home/[...slug]")?',
      default: "Home/index",
      validate: (value) => {
        if (/.+/.test(value)) {
          return componentExists(process.cwd() + `/pages/` + value)
            ? "A page, component or container with this name already exists"
            : true;
        }

        return "The route is required";
      },
    },
    {
      type: "confirm",
      name: "wantTS",
      default: true,
      message: "Do you want it to be on TypeScript?",
    },
    {
      type: "confirm",
      name: "wantMessages",
      default: true,
      message: "Do you want i18n messages (i.e. will this screen use text)?",
    },
    {
      type: "confirm",
      name: "wantStyle",
      default: true,
      message: "Do you want Style (i.e. will this screen use style)?",
    },
    {
      type: "confirm",
      name: "wantLoadable",
      default: false,
      message: "Do you want to load the screen asynchronously?",
    },
  ],
  actions: (data) => {
    const pagePath = NEXT_PATH + "/pages";
    const screenPath = NEXT_PATH + `/screens/{{properCase name}}Screen`;
    const templatePath = "./pageWithScreen/templates/";

    const actions = [
      {
        type: "add",
        path: pagePath + `/{{route}}.${data.wantTS ? "tsx" : "js"}`,
        templateFile: templatePath + "index.js.hbs",
        abortOnFail: true,
      },
      {
        type: "add",
        path: screenPath + `/index.${data.wantTS ? "tsx" : "js"}`,
        templateFile: templatePath + "screen.js.hbs",
        abortOnFail: true,
      },
      {
        type: "add",
        path: screenPath + `/tests/index.test.${data.wantTS ? "ts" : "js"}`,
        templateFile: templatePath + "test.js.hbs",
        abortOnFail: true,
      },
    ];

    if (data.wantMessages) {
      actions.push({
        type: "add",
        path: screenPath + `/messages.${data.wantTS ? "ts" : "js"}`,
        templateFile: templatePath + "messages.js.hbs",
        abortOnFail: true,
      });
    }
    if (data.wantStyle) {
      actions.push({
        type: "add",
        path: screenPath + `/Styled.${data.wantTS ? "ts" : "js"}`,
        templateFile: templatePath + "styled.js.hbs",
        abortOnFail: true,
      });
    }

    // If want Loadable.js to load the component asynchronously
    if (data.wantLoadable) {
      actions.push({
        type: "add",
        path: screenPath + `/Loadable.${data.wantTS ? "ts" : "js"}`,
        templateFile: templatePath + "loadable.js.hbs",
        abortOnFail: true,
      });
    }

    actions.push({
      type: "prettify",
      path: "/pages/",
    });

    return actions;
  },
};
