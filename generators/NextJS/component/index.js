/**
 * Component Generator
 */

"use strict";

const { NEXT_PATH } = require("../../../common");
const componentExists = require("../../../utils/componentExists");

module.exports = {
  description: "Add a component",
  prompts: [
    {
      type: "input",
      name: "name",
      message: "What should it be called?",
      default: "Button",
      validate: (value) => {
        if (/.+/.test(value)) {
          return componentExists(value)
            ? "A component or container with this name already exists"
            : true;
        }

        return "The name is required";
      },
    },
    {
      type: "confirm",
      name: "wantTS",
      default: true,
      message: "Do you want TypeScript?",
    },
    {
      type: "confirm",
      name: "wantProps",
      default: true,
      message: "Do you want your component to recieve Props?",
    },
    {
      type: "confirm",
      name: "wantMessages",
      default: true,
      message: "Do you want i18n messages (i.e. will this component use text)?",
    },
    {
      type: "confirm",
      name: "wantStyle",
      default: true,
      message: "Do you want Style (i.e. will this component use style)?",
    },
    {
      type: "confirm",
      name: "wantLoadable",
      default: false,
      message: "Do you want to load the component asynchronously?",
    },
    {
      type: "input",
      name: "pathPrefix",
      default: "",
      message: "Do you want nested path?",
    },
  ],
  actions: (data) => {
    // Generate index.js and index.test.js
    const templatePath = "./component/templates";
    const generatePath = NEXT_PATH + "/components" + data.pathPrefix;

    const actions = [
      {
        type: "add",
        path:
          generatePath +
          `/{{properCase name}}/index.${data.wantTS ? "tsx" : "js"}`,
        templateFile: templatePath + "index.js.hbs",
        abortOnFail: true,
      },
      {
        type: "add",
        path:
          generatePath +
          `/{{properCase name}}/tests/index.test.${data.wantTS ? "ts" : "js"}`,
        templateFile: templatePath + "test.js.hbs",
        abortOnFail: true,
      },
    ];

    // If they want a i18n messages file
    if (data.wantMessages) {
      actions.push({
        type: "add",
        path:
          generatePath +
          `/{{properCase name}}/messages.${data.wantTS ? "ts" : "js"}`,
        templateFile: templatePath + "messages.js.hbs",
        abortOnFail: true,
      });
    }
    if (data.wantStyle) {
      actions.push({
        type: "add",
        path:
          generatePath +
          `/{{properCase name}}/Styled.${data.wantTS ? "ts" : "js"}`,
        templateFile: templatePath + "Styled.js.hbs",
        abortOnFail: true,
      });
    }

    // If want Loadable.js to load the component asynchronously
    if (data.wantLoadable) {
      actions.push({
        type: "add",
        path:
          generatePath +
          `/{{properCase name}}/Loadable.${data.wantTS ? "ts" : "js"}`,
        templateFile: templatePath + "loadable.js.hbs",
        abortOnFail: true,
      });
    }

    // @ts-ignore
    actions.push({
      type: "prettify",
      path: "/components/",
    });

    return actions;
  },
};
