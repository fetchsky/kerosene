/**
 * Component Generator
 */
const { NEXT_PATH } = require("../../../common");
const componentExists = require("../../../utils/componentExists");

module.exports = {
  description: "Add new page in your next app",
  prompts: [
    {
      type: "input",
      name: "name",
      message: "What should it be called?",
      default: "Home",
    },
    {
      type: "input",
      name: "route",
      message:
        'Enter the route for page (e.g : "Home/index" or "Home/[...slug]")?',
      default: "Home/index",
      validate: (value) => {
        if (/.+/.test(value)) {
          return componentExists(process.cwd() + `/pages/` + value)
            ? "A page, component or container with this name already exists"
            : true;
        }

        return "The route is required";
      },
    },
    {
      type: "confirm",
      name: "wantTS",
      default: true,
      message: "Do you want it to be on TypeScript?",
    },
  ],
  actions: (data) => {
    const templatePath = "./pageWithoutScreen/templates/";
    const actions = [
      {
        type: "add",
        path: NEXT_PATH + `/pages/{{route}}.${data.wantTS ? "tsx" : "js"}`,
        templateFile: templatePath + "index.js.hbs",
        abortOnFail: true,
      },
    ];

    actions.push({
      type: "prettify",
      path: "/pages/",
    });

    return actions;
  },
};
