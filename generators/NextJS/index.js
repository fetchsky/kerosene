/**
 * React Templated Element Generator
 *
 * Exports the generators so plop knows them
 */
const path = require("path");
const { exec } = require("child_process");

const componentGenerator = require("./component/index.js");
const PageGenerator = require("./pageWithoutScreen/index");
const PageScreenGenerator = require("./pageWithScreen/index");

module.exports = (plop) => {
  plop.setGenerator("Page (with screen)", PageScreenGenerator);
  plop.setGenerator("Page (without screen)", PageGenerator);
  plop.setGenerator("Component", componentGenerator);

  plop.addHelper("curly", (object, open) => (open ? "{" : "}"));
  plop.setActionType("prettify", (answers, config) => {
    const folderPath = `${path.join(
      // __dirname,
      // '/../../app/',
      config.path,
      plop.getHelper("properCase")(answers.name) + (config.nameSuffix || ""),
      "**.js"
    )}`;
    exec(`npm run prettify -- "${folderPath}"`);
    return folderPath;
  });
};
