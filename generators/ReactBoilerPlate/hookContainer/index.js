/**
 * Container Generator
 */

const { APP_PATH } = require("../../../common");
const componentExists = require("../../../utils/componentExists");

module.exports = {
  description: "Add a container component",
  prompts: [
    {
      type: "input",
      name: "name",
      message: "What should it be called?",
      default: "Categories",
      validate: (value) => {
        if (/.+/.test(value)) {
          return componentExists(value)
            ? "A component or container with this name already exists"
            : true;
        }

        return "The name is required";
      },
    },
    {
      type: "confirm",
      name: "memo",
      default: true,
      message: "Do you want to wrap your component in React.memo?",
    },
    {
      type: "confirm",
      name: "wantActionsAndReducer",
      default: true,
      message:
        "Do you want an actions/constants/selectors/reducer tuple for this container?",
    },
    {
      type: "confirm",
      name: "wantSaga",
      default: true,
      message: "Do you want sagas for asynchronous flows? (e.g. fetching data)",
    },
    {
      type: "confirm",
      name: "wantLoadable",
      default: true,
      message: "Do you want to load resources asynchronously?",
    },
    {
      type: "confirm",
      name: "wantApi",
      default: true,
      message: "Do you want to add api to this container?",
    },
  ],
  actions: (data) => {
    const actions = [
      {
        type: "add",
        path: APP_PATH + "/containers/{{properCase name}}/tests/index.test.js",
        templateFile: "./hookContainer/test.js.hbs",
        abortOnFail: true,
      },
      {
        type: "add",
        path: APP_PATH + "/containers/{{properCase name}}/index.js",
        templateFile: "./hookContainer/index.js.hbs",
        abortOnFail: true,
      },
    ];

    // If they want actions and a reducer, generate actions.js, constants.js,
    // reducer.js and the corresponding tests for actions and the reducer
    if (data.wantActionsAndReducer) {
      // Actions
      actions.push({
        type: "add",
        path: APP_PATH + "/containers/{{properCase name}}/actions.js",
        templateFile: "./hookContainer/actions.js.hbs",
        abortOnFail: true,
      });
      actions.push({
        type: "add",
        path:
          APP_PATH + "/containers/{{properCase name}}/tests/actions.test.js",
        templateFile: "./hookContainer/actions.test.js.hbs",
        abortOnFail: true,
      });

      // Constants
      actions.push({
        type: "add",
        path: APP_PATH + "/containers/{{properCase name}}/constants.js",
        templateFile: "./hookContainer/constants.js.hbs",
        abortOnFail: true,
      });

      // Selectors
      actions.push({
        type: "add",
        path: APP_PATH + "/containers/{{properCase name}}/selectors.js",
        templateFile: "./hookContainer/selectors.js.hbs",
        abortOnFail: true,
      });
      actions.push({
        type: "add",
        path:
          APP_PATH + "/containers/{{properCase name}}/tests/selectors.test.js",
        templateFile: "./hookContainer/selectors.test.js.hbs",
        abortOnFail: true,
      });

      // Reducer
      actions.push({
        type: "add",
        path: APP_PATH + "/containers/{{properCase name}}/reducer.js",
        templateFile: "./hookContainer/reducer.js.hbs",
        abortOnFail: true,
      });
      actions.push({
        type: "add",
        path:
          APP_PATH + "/containers/{{properCase name}}/tests/reducer.test.js",
        templateFile: "./hookContainer/reducer.test.js.hbs",
        abortOnFail: true,
      });
    }

    // Sagas
    if (data.wantSaga) {
      actions.push({
        type: "add",
        path: APP_PATH + "/containers/{{properCase name}}/saga.js",
        templateFile: "./hookContainer/saga.js.hbs",
        abortOnFail: true,
      });
      actions.push({
        type: "add",
        path: APP_PATH + "/containers/{{properCase name}}/tests/saga.test.js",
        templateFile: "./hookContainer/saga.test.js.hbs",
        abortOnFail: true,
      });
    }

    if (data.wantLoadable) {
      actions.push({
        type: "add",
        path: APP_PATH + "/containers/{{properCase name}}/Loadable.js",
        templateFile: "./component/loadable.js.hbs",
        abortOnFail: true,
      });
    }

    if (data.wantApi) {
      actions.push({
        type: "add",
        path: APP_PATH + "/containers/{{properCase name}}/api.js",
        templateFile: "./hookContainer/api.js.hbs",
        abortOnFail: true,
      });
    }

    actions.push({
      type: "prettify",
      path: "/containers/",
    });

    return actions;
  },
};
