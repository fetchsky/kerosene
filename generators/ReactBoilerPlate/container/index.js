/**
 * Container Generator
 */

const componentExists = require("../../../utils/componentExists");

module.exports = {
  description: "Add a container component",
  prompts: [
    {
      type: "list",
      name: "component",
      message: "Select the base component type:",
      default: "React.PureComponent",
      choices: () => ["React.PureComponent", "React.Component"],
    },
    {
      type: "input",
      name: "name",
      message: "What should it be called?",
      default: "Form",
      validate: (value) => {
        if (/.+/.test(value)) {
          return componentExists(value)
            ? "A page/component/container with this name already exists"
            : true;
        }
        return "The name is required";
      },
    },
    {
      type: "confirm",
      name: "wantActionsAndReducer",
      default: true,
      message:
        "Do you want an actions/constants/selectors/reducer tuple for this container?",
    },
    {
      type: "confirm",
      name: "wantSaga",
      default: true,
      message: "Do you want sagas for asynchronous flows? (e.g. fetching data)",
    },
    {
      type: "confirm",
      name: "wantLoadable",
      default: true,
      message: "Do you want to load resources asynchronously?",
    },
    {
      type: "confirm",
      name: "wantApi",
      default: true,
      message: "Do you want to add api to this container?",
    },
    {
      type: "input",
      name: "pathPrefix",
      default: process.cwd() + "/app/containers",
      message: "Do you want to update path?",
    },
  ],
  actions: (data) => {
    const actions = [
      {
        type: "add",
        path: data.pathPrefix + "/{{properCase name}}/index.js",
        templateFile: "./container/index.js.hbs",
        abortOnFail: true,
      },
      {
        type: "add",
        path: data.pathPrefix + "/{{properCase name}}/tests/index.test.js",
        templateFile: "./container/test.js.hbs",
        abortOnFail: true,
      },
    ];

    // If they want actions and a reducer, generate actions.js, constants.js,
    // reducer.js and the corresponding tests for actions and the reducer
    if (data.wantActionsAndReducer) {
      // Actions
      actions.push({
        type: "add",
        path: data.pathPrefix + "/{{properCase name}}/actions.js",
        templateFile: "./container/actions.js.hbs",
        abortOnFail: true,
      });
      actions.push({
        type: "add",
        path: data.pathPrefix + "/{{properCase name}}/tests/actions.test.js",
        templateFile: "./container/actions.test.js.hbs",
        abortOnFail: true,
      });

      // Constants
      actions.push({
        type: "add",
        path: data.pathPrefix + "/{{properCase name}}/constants.js",
        templateFile: "./container/constants.js.hbs",
        abortOnFail: true,
      });

      // Selectors
      actions.push({
        type: "add",
        path: data.pathPrefix + "/{{properCase name}}/selectors.js",
        templateFile: "./container/selectors.js.hbs",
        abortOnFail: true,
      });
      actions.push({
        type: "add",
        path: data.pathPrefix + "/{{properCase name}}/tests/selectors.test.js",
        templateFile: "./container/selectors.test.js.hbs",
        abortOnFail: true,
      });

      // Reducer
      actions.push({
        type: "add",
        path: data.pathPrefix + "/{{properCase name}}/reducer.js",
        templateFile: "./container/reducer.js.hbs",
        abortOnFail: true,
      });
      actions.push({
        type: "add",
        path: data.pathPrefix + "/{{properCase name}}/tests/reducer.test.js",
        templateFile: "./container/reducer.test.js.hbs",
        abortOnFail: true,
      });
    }

    // Sagas
    if (data.wantSaga) {
      actions.push({
        type: "add",
        path: data.pathPrefix + "/{{properCase name}}/saga.js",
        templateFile: "./container/saga.js.hbs",
        abortOnFail: true,
      });
      actions.push({
        type: "add",
        path: data.pathPrefix + "/{{properCase name}}/tests/saga.test.js",
        templateFile: "./container/saga.test.js.hbs",
        abortOnFail: true,
      });
    }

    if (data.wantLoadable) {
      actions.push({
        type: "add",
        path: data.pathPrefix + "/{{properCase name}}/Loadable.js",
        templateFile: "./component/loadable.js.hbs",
        abortOnFail: true,
      });
    }

    if (data.wantApi) {
      actions.push({
        type: "add",
        path: data.pathPrefix + "/{{properCase name}}/api.js",
        templateFile: "./container/api.js.hbs",
        abortOnFail: true,
      });
    }

    // @ts-ignore
    actions.push({
      type: "prettify",
      path: data.pathPrefix,
    });

    return actions;
  },
};
