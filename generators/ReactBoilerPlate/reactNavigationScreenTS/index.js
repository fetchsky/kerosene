/**
 * Component Generator
 */

/* eslint strict: ["off"] */

"use strict";

const componentExists = require("../../../utils/componentExists");

module.exports = {
  description: "Add a TS React Navigation Screen container",
  prompts: [
    {
      type: "input",
      name: "name",
      message: "What should it be called?",
      default: "Home",
      validate: (value) => {
        if (/.+/.test(value)) {
          return componentExists(`${value}Screen`)
            ? "A screen, component or container with this name already exists"
            : true;
        }

        return "The name is required";
      },
    },
    {
      type: "confirm",
      name: "wantStyle",
      default: true,
      message: "Do you want styles for styling component?",
    },
    {
      type: "confirm",
      name: "wantMessages",
      default: true,
      message: "Do you want i18n messages (i.e. will this component use text)?",
    },
    {
      type: "input",
      name: "pathPrefix",
      default: process.cwd() + "/app/screens",
      message: "Do you want to update path?",
    },
  ],
  actions: (data) => {
    const actions = [
      {
        type: "add",
        path: data.pathPrefix + "/{{properCase name}}Screen/index.tsx",
        templateFile: "./reactNavigationScreenTS/index.js.hbs",
        abortOnFail: true,
      },
      {
        type: "add",
        path: data.pathPrefix + "/{{properCase name}}Screen/types.ts",
        templateFile: "./reactNavigationScreenTS/types.js.hbs",
        abortOnFail: true,
      },
      {
        type: "add",
        path:
          data.pathPrefix + "/{{properCase name}}Screen/tests/index.test.ts",
        templateFile: "./reactNavigationScreenTS/test.js.hbs",
        abortOnFail: true,
      },
      {
        type: "add",
        path: data.pathPrefix + "/{{properCase name}}Screen/Loadable.ts",
        templateFile: "./reactNavigationScreenTS/loadable.js.hbs",
        abortOnFail: true,
      },
    ];

    // If component wants messages
    if (data.wantMessages) {
      actions.push({
        type: "add",
        path: data.pathPrefix + "/{{properCase name}}Screen/messages.ts",
        templateFile: "./reactNavigationScreenTS/messages.js.hbs",
        abortOnFail: true,
      });
    }

    if (data.wantStyle) {
      actions.push({
        type: "add",
        path: data.pathPrefix + "/{{properCase name}}Screen/style.ts",
        templateFile: "./reactNavigationScreenTS/style.js.hbs",
        abortOnFail: true,
      });
    }

    // @ts-ignore
    actions.push({
      type: "prettify",
      path: "/screens/",
    });

    return actions;
  },
};
