/**
 * Container Generator
 */

const { APP_PATH } = require("../../../common");
const componentExists = require("../../../utils/componentExists");

module.exports = {
  description:
    "Add a redux toolkit container component with typescript [Key Based]",
  prompts: [
    {
      type: "input",
      name: "name",
      message: "What should it be called?",
      default: "Categories",
      validate: (value) => {
        if (/.+/.test(value)) {
          return componentExists(value)
            ? "A component or container with this name already exists"
            : true;
        }
        return "The name is required";
      },
    },
    {
      type: "confirm",
      name: "wantSaga",
      default: true,
      message: "Do you want sagas for asynchronous flows? (e.g. fetching data)",
    },
    {
      type: "confirm",
      name: "wantLoadable",
      default: true,
      message: "Do you want to load resources asynchronously?",
    },
    {
      type: "confirm",
      name: "wantApi",
      default: true,
      message: "Do you want to add api to this container?",
    },
    {
      type: "confirm",
      name: "isKeyBased",
      default: true,
      message: "Do you want this duck to be key based?",
    },
  ],
  actions: (data) => {
    const actions = [
      {
        type: "add",
        path: APP_PATH + "/containers/{{properCase name}}/tests/index.test.js",
        templateFile: "./duxTS/test.js.hbs",
        abortOnFail: true,
      },
      {
        type: "add",
        path: APP_PATH + "/containers/{{properCase name}}/index.tsx",
        templateFile: "./duxTS/index.tsx.hbs",
        abortOnFail: true,
      },
      {
        type: "add",
        path: APP_PATH + "/containers/{{properCase name}}/slice.ts",
        templateFile: "./duxTS/slice.ts.hbs",
        abortOnFail: true,
      },
      {
        type: "add",
        path: APP_PATH + "/containers/{{properCase name}}/selectors.ts",
        templateFile: "./duxTS/selectors.ts.hbs",
        abortOnFail: true,
      },
      {
        type: "add",
        path:
          APP_PATH + "/containers/{{properCase name}}/tests/selectors.test.js",
        templateFile: "./duxTS/selectors.test.js.hbs",
        abortOnFail: true,
      },
      {
        type: "add",
        path: APP_PATH + "/containers/{{properCase name}}/loadable.ts",
        templateFile: "./component/loadable.js.hbs",
        abortOnFail: true,
      },
      {
        type: "add",
        path: APP_PATH + "/containers/{{properCase name}}/types.ts",
        templateFile: "./duxTS/types.ts.hbs",
        abortOnFail: true,
      },
    ];

    // Sagas
    if (data.wantSaga) {
      actions.push({
        type: "add",
        path: APP_PATH + "/containers/{{properCase name}}/saga.ts",
        templateFile: "./duxTS/saga.ts.hbs",
        abortOnFail: true,
      });
      actions.push({
        type: "add",
        path: APP_PATH + "/containers/{{properCase name}}/tests/saga.test.js",
        templateFile: "./duxTS/saga.test.js.hbs",
        abortOnFail: true,
      });
    }

    if (data.wantApi) {
      actions.push({
        type: "add",
        path: APP_PATH + "/containers/{{properCase name}}/api.ts",
        templateFile: "./duxTS/api.ts.hbs",
        abortOnFail: true,
      });
    }

    actions.push({
      type: "prettify",
      path: "/containers/",
    });

    return actions;
  },
};
