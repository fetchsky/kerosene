/**
 * React Templated Element Generator
 *
 * Exports the generators so plop knows them
 */
const path = require("path");
const { exec } = require("child_process");

const pageGenerator = require("./page/index");
const screenGenerator = require("./screen/index");
const reactNavigationScreenTSGenerator = require("./reactNavigationScreenTS/index");
const componentGenerator = require("./component/index.js");
const containerGenerator = require("./container/index.js");
const hookContainerGenerator = require("./hookContainer/index.js");
const duxTSCRUDContainerGenerator = require("./duxTSCRUDContainer/index.js");
const duxGenerator = require("./dux/index.js");
const duxTSGenerator = require("./duxTS/index.js");
const languageGenerator = require("./language/index.js");

module.exports = (plop) => {
  plop.setGenerator("RBP Page", pageGenerator);
  plop.setGenerator("RBP-RN Screen", screenGenerator);
  plop.setGenerator(
    "RBP React Navigation TS Screen",
    reactNavigationScreenTSGenerator
  );
  plop.setGenerator("RBP Component", componentGenerator);
  plop.setGenerator("RBP Container", containerGenerator);
  plop.setGenerator("Hooks Container", hookContainerGenerator);
  plop.setGenerator("DuxTS CRUD Container", duxTSCRUDContainerGenerator);
  plop.setGenerator("Dux", duxGenerator);
  plop.setGenerator("DuxTS", duxTSGenerator);
  plop.setGenerator("RBP Language", languageGenerator);
  plop.addHelper("curly", (object, open) => (open ? "{" : "}"));
  plop.setActionType("prettify", (answers, config) => {
    const folderPath = `${path.join(
      // __dirname,
      // '/../../app/',
      config.path,
      plop.getHelper("properCase")(answers.name) + (config.nameSuffix || ""),
      "**.js"
    )}`;
    exec(`npm run prettify -- "${folderPath}"`);
    return folderPath;
  });
};
