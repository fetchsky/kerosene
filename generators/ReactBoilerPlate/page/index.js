/**
 * Component Generator
 */

/* eslint strict: ["off"] */

"use strict";

const componentExists = require("../../../utils/componentExists");

module.exports = {
  description: "Add new page in your react app",
  prompts: [
    {
      type: "input",
      name: "name",
      message: "What should it be called?",
      default: "Home",
      validate: (value) => {
        if (/.+/.test(value)) {
          return componentExists(`${value}Page`)
            ? "A page, component or container with this name already exists"
            : true;
        }

        return "The name is required";
      },
    },
    {
      type: "confirm",
      name: "wantHeaders",
      default: true,
      message: "Do you want headers?",
    },
    {
      type: "confirm",
      name: "wantStyle",
      default: true,
      message: "Do you want styles for styling component?",
    },
    {
      type: "confirm",
      name: "wantMessages",
      default: true,
      message: "Do you want i18n messages (i.e. will this component use text)?",
    },
    {
      type: "confirm",
      name: "wantLoadable",
      default: true,
      message: "Do you want this component to be Loadable?",
    },
    {
      type: "input",
      name: "pathPrefix",
      default: process.cwd() + "/app/pages",
      message: "Do you want to update path?",
    },
  ],
  actions: (data) => {
    const actions = [
      {
        type: "add",
        path: data.pathPrefix + "/{{properCase name}}/index.js",
        templateFile: "./page/index.js.hbs",
        abortOnFail: true,
      },
      {
        type: "add",
        path: data.pathPrefix + "/{{properCase name}}/tests/index.test.js",
        templateFile: "./page/test.js.hbs",
        abortOnFail: true,
      },
    ];

    // If component wants messages
    if (data.wantMessages) {
      actions.push({
        type: "add",
        path: data.pathPrefix + "/{{properCase name}}/messages.js",
        templateFile: "./page/messages.js.hbs",
        abortOnFail: true,
      });
    }

    // If they want a Loadable file
    if (data.wantLoadable) {
      actions.push({
        type: "add",
        path: data.pathPrefix + "/{{properCase name}}/Loadable.js",
        templateFile: "./page/loadable.js.hbs",
        abortOnFail: true,
      });
    }

    if (data.wantStyle) {
      actions.push({
        type: "add",
        path: data.pathPrefix + "/{{properCase name}}/Styled.js",
        templateFile: "./page/styled.js.hbs",
        abortOnFail: true,
      });
    }

    // @ts-ignore
    actions.push({
      type: "prettify",
      path: "/pages/",
    });

    return actions;
  },
};
