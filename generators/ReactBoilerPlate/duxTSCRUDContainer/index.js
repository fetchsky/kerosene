/**
 * Container Generator
 */

const { APP_PATH } = require("../../../common");
const componentExists = require("../../../utils/componentExists");

module.exports = {
  description:
    "Add a redux toolkit container component with typescript [Key Based]",
  prompts: [
    {
      type: "input",
      name: "name",
      message: "What should it be called?",
      default: "Categories",
      validate: (value) => {
        if (/.+/.test(value)) {
          return componentExists(value)
            ? "A component or container with this name already exists"
            : true;
        }
        return "The name is required";
      },
    },
    {
      type: "confirm",
      name: "wantLoadable",
      default: true,
      message: "Do you want to load resources asynchronously?",
    },
  ],
  actions: (data) => {
    const actions = [
      {
        type: "add",
        path: APP_PATH + "/containers/{{properCase name}}/tests/index.test.js",
        templateFile: "./duxTSCRUDContainer/test.js.hbs",
        abortOnFail: true,
      },
      {
        type: "add",
        path: APP_PATH + "/containers/{{properCase name}}/index.tsx",
        templateFile: "./duxTSCRUDContainer/index.tsx.hbs",
        abortOnFail: true,
      },
      {
        type: "add",
        path: APP_PATH + "/containers/{{properCase name}}/slice.ts",
        templateFile: "./duxTSCRUDContainer/slice.ts.hbs",
        abortOnFail: true,
      },
      {
        type: "add",
        path: APP_PATH + "/containers/{{properCase name}}/selectors.ts",
        templateFile: "./duxTSCRUDContainer/selectors.ts.hbs",
        abortOnFail: true,
      },
      {
        type: "add",
        path:
          APP_PATH + "/containers/{{properCase name}}/tests/selectors.test.js",
        templateFile: "./duxTSCRUDContainer/selectors.test.js.hbs",
        abortOnFail: true,
      },
      {
        type: "add",
        path: APP_PATH + "/containers/{{properCase name}}/types.ts",
        templateFile: "./duxTSCRUDContainer/types.ts.hbs",
        abortOnFail: true,
      },
      {
        type: "add",
        path: APP_PATH + "/containers/{{properCase name}}/api.ts",
        templateFile: "./duxTSCRUDContainer/api.ts.hbs",
        abortOnFail: true,
      },
      {
        type: "add",
        path: APP_PATH + "/containers/{{properCase name}}/saga.ts",
        templateFile: "./duxTSCRUDContainer/saga.ts.hbs",
        abortOnFail: true,
      },
      {
        type: "add",
        path: APP_PATH + "/containers/{{properCase name}}/tests/saga.test.js",
        templateFile: "./duxTSCRUDContainer/saga.test.js.hbs",
        abortOnFail: true,
      },
    ];

    if (data.wantLoadable) {
      actions.push({
        type: "add",
        path: APP_PATH + "/containers/{{properCase name}}/loadable.ts",
        templateFile: "./component/loadable.js.hbs",
        abortOnFail: true,
      });
    }

    actions.push({
      type: "prettify",
      path: "/containers/",
    });

    return actions;
  },
};
