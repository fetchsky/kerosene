const fse = require("fs-extra");
const inquirer = require("inquirer");
const nodePlop = require("node-plop");
const SwaggerClient = require("swagger-client");
const openapiTS = require("openapi-typescript").default;

const successResponseCode = {
  get: 200,
  post: 201,
  patch: 200,
  delete: 200,
};

const successBulkResponseCode = {
  delete: 200,
  create: 201,
};

const bulkTypeMap = {
  delete: "REMOVE_BULK",
  create: "CREATE_BULK",
};

const defaultRouteMap = {
  get: "FETCH",
  post: "CREATE",
};

const detailRouteMap = {
  get: "DETAIL",
  patch: "UPDATE",
  delete: "REMOVE",
};

function isDetailRoute(path) {
  return path.indexOf("{") !== -1 && path.indexOf("}") !== -1;
}

inquirer
  .prompt([
    {
      type: "input",
      name: "fileURL",
      message: "What's your swaggers json file URL?",
    },
    {
      type: "input",
      name: "outputPath",
      default: "/providers",
      message: "Do you want to override path?",
    },
  ])
  .then((answers) => {
    (async function() {
      // create swagger client
      const jsonFileURL = answers.fileURL;
      const plop = await nodePlop(`${__dirname}/plop.js`);
      const generator = plop.getGenerator("reactQueryProvider");

      const swagger = await SwaggerClient(jsonFileURL);
      if (swagger) {
        const types = await openapiTS(jsonFileURL);
        // create types file
        const typesDir =
          process.cwd() + answers.outputPath + "/providerTypes.ts";
        fse.removeSync(typesDir);
        fse.outputFileSync(typesDir, types);

        const providersList = Object.keys(swagger.apis || {}).map(
          (provider) => ({
            id: provider,
            name: provider.replace(/\s/g, ""),
            methods: [],
          })
        );

        if (swagger.spec && swagger.spec.paths) {
          for (const path of Object.keys(swagger.spec.paths || {})) {
            if (swagger.spec.paths[path]) {
              const splittedPath = path.split("/");
              const bulkType = splittedPath[splittedPath.length - 2];
              const lastSection = splittedPath[splittedPath.length - 1];

              if (
                lastSection === "bulk" &&
                swagger.spec.paths[path].post &&
                swagger.spec.paths[path].post.tags &&
                swagger.spec.paths[path].post.tags.length
              ) {
                const index = providersList.findIndex(
                  (el) => el.id === swagger.spec.paths[path].post.tags[0]
                );

                providersList[index].methods.push({
                  path,
                  type: bulkTypeMap[bulkType],
                  method: "POST",
                  codes: {
                    success: successBulkResponseCode[bulkType],
                  },
                  operationId:
                    swagger.spec.paths[path].post.operationId || "-1",
                });
              } else {
                for (const method of Object.keys(
                  swagger.spec.paths[path] || {}
                )) {
                  if (
                    swagger.spec.paths[path][method] &&
                    swagger.spec.paths[path][method].tags &&
                    swagger.spec.paths[path][method].tags.length
                  ) {
                    const index = providersList.findIndex(
                      (el) => el.id === swagger.spec.paths[path][method].tags[0]
                    );
                    providersList[index].methods.push({
                      path,
                      type: isDetailRoute(lastSection)
                        ? detailRouteMap[method]
                        : defaultRouteMap[method],
                      method: method.toUpperCase(),
                      codes: {
                        success: successResponseCode[method],
                      },
                      operationId:
                        swagger.spec.paths[path][method].operationId || "-1",
                    });
                  }
                }
              }
            }
          }
        }
        inquirer
          .prompt([
            {
              name: "providers",
              type: "checkbox",
              message: "Select Provider",
              choices: providersList.map((obj) => obj.id),
            },
          ])
          .then((providerAnswers) => {
            const filterProviderList = providersList.filter((el) =>
              providerAnswers.providers.includes(el.id)
            );
            for (const provider of filterProviderList) {
              fse.removeSync(
                process.cwd() + answers.outputPath + "/" + provider.name
              );
              generator.runActions({
                provider,
                outputPath: answers.outputPath,
              });
            }
          });
      }
    })();
  });
