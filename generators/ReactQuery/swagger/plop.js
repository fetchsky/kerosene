const path = require("path");
const { exec } = require("child_process");

function generateTypeCheck(provider = {}) {
  const res = {};
  if (provider && provider.methods && provider.methods.length) {
    for (const method of provider.methods) {
      if (method.type) {
        res[method.type.toLowerCase()] = true;
      }
    }
  }
  return res;
}

module.exports = (plop) => {
  plop.setActionType("prettify", (answers, config) => {
    const folderPath = `${path.join(
      // __dirname,
      // '/../../app/',
      config.path,
      plop.getHelper("properCase")(answers.name) + (config.nameSuffix || ""),
      "**.ts"
    )}`;
    exec(`npm run prettify -- "${folderPath}"`);
    return folderPath;
  });
  plop.setGenerator("reactQueryProvider", {
    description: "React Query Generator",
    prompts: [],
    actions: (data) => {
      const templatePath = "../templates";

      const actions = [
        {
          type: "add",
          path:
            process.cwd() +
            data.outputPath +
            "/" +
            data.provider.name +
            "/index.ts",
          templateFile: templatePath + "/index.js.hbs",
          abortOnFail: true,
          data: {
            name: data.provider.name,
            ...generateTypeCheck(data.provider),
          },
        },
        {
          type: "add",
          path:
            process.cwd() +
            data.outputPath +
            "/" +
            data.provider.name +
            "/api.ts",
          templateFile: templatePath + "/api.js.hbs",
          abortOnFail: true,
          data: {
            name: data.provider.name,
            initial: true,
          },
        },
        {
          type: "add",
          path:
            process.cwd() +
            data.outputPath +
            "/" +
            data.provider.name +
            "/types.ts",
          templateFile: templatePath + "/swaggerTypes.js.hbs",
          abortOnFail: true,
          data: {
            initial: true,
            name: data.provider.name,
          },
        },
      ];

      // Types
      if (
        data.provider &&
        data.provider.methods &&
        data.provider.methods.length
      ) {
        for (const el of data.provider.methods) {
          actions.push({
            type: "append",
            path:
              process.cwd() +
              data.outputPath +
              "/" +
              data.provider.name +
              "/api.ts",
            templateFile: templatePath + "/api.js.hbs",
            abortOnFail: true,
            data: {
              name: data.provider.name,
              [el.type.toLowerCase()]: true,
              ...el,
              path: el.path.replace(/{/g, "${props."),
            },
          });
          actions.push({
            type: "append",
            path:
              process.cwd() +
              data.outputPath +
              "/" +
              data.provider.name +
              "/types.ts",
            templateFile: templatePath + "/swaggerTypes.js.hbs",
            abortOnFail: true,
            data: {
              name: data.provider.name,
              [el.type.toLowerCase()]: true,
              ...el,
            },
          });
        }

        // Types End
        actions.push({
          type: "append",
          path:
            process.cwd() +
            data.outputPath +
            "/" +
            data.provider.name +
            "/types.ts",
          templateFile: templatePath + "/swaggerTypes.js.hbs",
          abortOnFail: true,
          data: {
            end: true,
          },
        });
      }

      // @ts-ignore
      actions.push({
        type: "prettify",
        path: process.cwd() + data.outputPath + "/" + data.provider.name,
      });
      return actions;
    },
  });
};
