"use strict";

const inquirer = require("inquirer");
var child = require("child_process");

inquirer
  .prompt([
    {
      type: "confirm",
      name: "swagger",
      message: "Do you want to generate providers using swagger?",
    },
  ])
  .then((answers) => {
    (async function() {
      if (answers.swagger) {
        child.execSync("node " + __dirname + "/swagger/index.js", {
          stdio: "inherit",
        });
      } else {
        child.execSync("plop --plopfile " + __dirname + "/plop.js", {
          stdio: "inherit",
        });
      }
    })();
  });
