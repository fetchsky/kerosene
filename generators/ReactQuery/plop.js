"use strict";

const path = require("path");
const { exec } = require("child_process");
const componentExists = require("../../utils/componentExists");

module.exports = (plop) => {
  plop.setActionType("prettify", (answers, config) => {
    const folderPath = `${path.join(
      // __dirname,
      // '/../../app/',
      config.path,
      plop.getHelper("properCase")(answers.name) + (config.nameSuffix || ""),
      "**.ts"
    )}`;
    exec(`npm run prettify -- "${folderPath}"`);
    return folderPath;
  });
  plop.setGenerator("reactQueryProvider", {
    description: "React Query Generator",
    prompts: [
      {
        type: "input",
        name: "name",
        message: "What should it be called?",
        default: "Country",
        validate: (value) => {
          if (/.+/.test(value)) {
            return componentExists(value)
              ? "A component or container with this name already exists"
              : true;
          }
          return "The name is required";
        },
      },
      {
        type: "input",
        name: "outputPath",
        default: "/providers",
        message: "Do you want to override path?",
      },
    ],
    actions: (data) => {
      const templatePath = "./templates";
      const generatePath = process.cwd() + data.outputPath + "/{{name}}";

      const actions = [
        {
          type: "add",
          path: generatePath + "/index.ts",
          templateFile: templatePath + "/index.js.hbs",
          abortOnFail: true,
          data: {
            name: data.name,
            fetch: true,
            detail: true,
            create: true,
            bulk: true,
            update: true,
            remove: true,
          },
        },
        {
          type: "add",
          path: generatePath + "/api.ts",
          templateFile: templatePath + "/api.js.hbs",
          abortOnFail: true,
          data: {
            initial: true,
            name: data.name,
            fetch: true,
            detail: true,
            create: true,
            bulk: true,
            update: true,
            remove: true,
          },
        },
        {
          type: "add",
          path: generatePath + "/types.ts",
          templateFile: templatePath + "/types.js.hbs",
          abortOnFail: true,
          data: {
            initial: true,
            name: data.name,
            fetch: true,
            detail: true,
            create: true,
            bulk: true,
            update: true,
            remove: true,
          },
        },
      ];

      // Types End
      actions.push({
        type: "append",
        path: generatePath + "/types.ts",
        templateFile: templatePath + "/types.js.hbs",
        abortOnFail: true,
        data: {
          end: true,
        },
      });

      // @ts-ignore
      actions.push({
        type: "prettify",
        path: process.cwd() + data.outputPath,
      });
      return actions;
    },
  });
};
