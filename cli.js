#!/usr/bin/env node

"use strict";
var inquirer = require("inquirer");
var child = require("child_process");

const pkg = require("./package.json");
require("please-upgrade-node")(
  Object.assign({}, pkg, {
    engines: {
      node: ">=10",
    },
  })
);

const NextPlopFile = __dirname + "/generators/NextJS/index.js";
const ReactPlopFile = __dirname + "/generators/ReactBoilerPlate/index.js";
const ReactQueryFile = __dirname + "/generators/ReactQuery/index.js";

inquirer
  .prompt([
    {
      type: "list",
      name: "type",
      message: "Select Generator for",
      choices: ["NextJS", "React Boiler Plate", "React Query"],
    },
  ])
  .then(({ type }) => {
    switch (type) {
      case "NextJS":
        child.execSync("plop --plopfile " + NextPlopFile, { stdio: "inherit" });
        return;
      case "React Boiler Plate":
        child.execSync("plop --plopfile " + ReactPlopFile, {
          stdio: "inherit",
        });
        return;
      case "React Query":
        child.execSync("node " + ReactQueryFile, {
          stdio: "inherit",
        });
        return;
      default:
    }
    console.error("No generators found for : ".red, type.blue);
  });
